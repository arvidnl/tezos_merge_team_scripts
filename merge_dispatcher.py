import argparse
import csv
import sys
from datetime import datetime

from gitlab.v4.objects import ProjectMergeRequest  # type: ignore

from util import eprint, init_gitlab, url_mr, url_issue

PROJECT_ID = 3836952


def action_merging(project, _args):
    eprint("Find MRs set to merge on successful pipeline...")

    potentially_mergable = project.mergerequests.list(
        all=True,
        state='opened',
        approver_ids='Any',
        wip='no',
    )

    to_merge = []
    for m in potentially_mergable:
        if m.merge_when_pipeline_succeeds:
            to_merge.append(m)

    eprint("MRs set to merge:")
    fmt = "{:<20} {:>10} {}"
    eprint(fmt.format("Id", "Title", "Url"))
    for m in to_merge:
        eprint(fmt.format(m.iid, m.title, url_mr(m)))


def mr_is_mergable(mergereq: ProjectMergeRequest):
    # todo:
    #  - [ ] find dependncies
    #  - [ ] todos?
    #  - [ ] commit history is clean
    #  - [/] no wip
    #  - [X] has two merge team approvers
    #  - [ ] needs rebase yes / no / has conflict
    #  - [ ] outstanding threads
    #  - [ ] open todos
    #  - [ ] set to merge

    # for c in m.commits():
    #     if any(c
    return (
        mergereq.blocking_discussions_resolved
        and mergereq.approvals.get().approved
    )


def action_mr_candidates(project, _args, output=sys.stdout):
    eprint("Finding MRs looking mergable...")

    potentially_mergable = project.mergerequests.list(
        all=True,
        state='opened',
        approver_ids='Any',
        wip='no',
    )

    hot_targets = filter(mr_is_mergable, potentially_mergable)

    fieldnames = [
        'iid',
        'title',
        'merge_when_pipeline_succeeds',
        'blocking_discussions_resolved',
        'url',
    ]
    writer = csv.DictWriter(output, fieldnames=fieldnames)
    writer.writeheader()
    for m in hot_targets:
        writer.writerow(
            {
                'iid': m.iid,
                'title': m.title,
                'merge_when_pipeline_succeeds': m.merge_when_pipeline_succeeds,
                'blocking_discussions_resolved': m.blocking_discussions_resolved,
                'url': url_mr(m),
            }
        )


def get_merge_team_members(project):
    rules = project.approvalrules.list()
    assert len(rules) == 1
    return rules[0].eligible_approvers


states = ['opened', 'closed', 'merged']
fields = {
    'member': 'Member',
    'assigned_opened': 'A. opened',
    # 'assigned_locked': 'A. locked',
    'assigned_closed': 'A. closed',
    'assigned_merged': 'A. merged',
    'reviewed_opened': 'R. opened',
    # 'reviewed_locked': 'R. locked',
    'reviewed_closed': 'R. closed',
    'reviewed_merged': 'R. merged',
}


def action_mr_stats(project, _args):
    eprint("Find merge stats...")

    rows = []
    for m in get_merge_team_members(project):
        member_name = m['name']
        eprint(f"Fetching mrs of {member_name}")
        row = {}
        row['member'] = m
        for s in states:
            k = 'assigned_' + s
            eprint(f'Fetching mrs of {member_name}: {k}')
            row[k] = project.mergerequests.list(
                assignee_id=m['id'], state=s, all=True
            )
        for s in states:
            k = 'reviewed_' + s
            eprint(f'Fetching mrs of {member_name}: {k}')
            row[k] = project.mergerequests.list(
                reviewer_id=m['id'], state=s, all=True
            )
        rows.append(row)

    # assigned_sorted = []
    # for (m, (mrs_open, mrs_other)) in assigned.items():
    #     assigned_sorted.append((m, mrs_open, mrs_other))
    # assigned_sorted.sort(key=lambda a: len(a[1]), reverse=True)

    fmt = "{:<20}" + ((len(fields) - 1) * " {:<10}")
    print(fmt.format(*fields.values()))
    for row in rows:
        member = row.pop('member')
        print(fmt.format(member['name'], *(map(len, row.values()))))


def get_closed(project, args):
    closed = project.mergerequests.list(
        # all=True,
        state='closed',
        # todo: does this work?
        labels=args.labels,
        updated_after=args.since.isoformat(),
    )
    return [
        m
        for m in closed
        if datetime.fromisoformat(m.closed_at[:-1]) >= args.since
    ]


def action_mr_closed(project, args):
    closed = get_closed(project, args)

    msg = f'MRs closed since {args.since.isoformat()} with labels {",".join(args.labels)}:'
    eprint(msg)
    fmt = "{:<5} {:<25} {:>10} {:>10} {}"
    eprint(fmt.format("Id", "Closed", "Title", "Labels", "Url"))
    for m in closed:
        eprint(
            fmt.format(
                m.iid,
                m.closed_at,
                m.title,
                ",".join(m.labels),
                url_mr(m),
            )
        )


def get_merged(project, args):
    merged = project.mergerequests.list(
        # all=True,
        state='merged',
        # todo: does this work?
        labels=args.labels,
        updated_after=args.since.isoformat(),
    )
    return [
        m
        for m in merged
        if datetime.fromisoformat(m.merged_at[:-1]) >= args.since
    ]


def action_mr_merged(project, args):
    merged = get_merged(project, args)
    msg = f'MRs merged since {args.since.isoformat()} with labels {",".join(args.labels)}:'
    eprint(msg)
    fmt = "{:<5} {:<25} {:>10} {:>10} {}"
    eprint(fmt.format("Id", "Merged", "Title", "Labels", "Url"))
    for m in merged:
        eprint(
            fmt.format(
                m.iid,
                m.merged_at,
                m.title,
                ",".join(m.labels),
                url_mr(m),
            )
        )


def get_active_issues(project, args):
    active = project.issues.list(
        all=True,
        labels=args.labels,
        updated_after=args.since.isoformat(),
        order_by='updated_at',
    )
    return [
        resource
        for resource in active
        if datetime.fromisoformat(resource.updated_at[:-1]) >= args.since
    ]


def action_issues_active(project, args):
    merged = get_active_issues(project, args)
    msg = f'Issues active since {args.since.isoformat()} with labels {",".join(args.labels)}:'
    eprint(msg)
    fmt = "{:<5} {:<24} {:>10} {:>25} {}"
    eprint(fmt.format("Id", "Updated", "State", "Title", "Labels", "Url"))
    for m in merged:
        eprint(
            fmt.format(
                m.iid,
                m.updated_at,
                m.state,
                m.title,
                ",".join(m.labels),
                url_issue(m),
            )
        )


def get_active_opened_mr(project, args):
    active = project.mergerequests.list(
        all=True,
        labels=args.labels,
        updated_after=args.since.isoformat(),
        state='opened',
        order_by='updated_at',
    )
    return [
        resource
        for resource in active
        if datetime.fromisoformat(resource.updated_at[:-1]) >= args.since
    ]


def action_mr_active_open(project, args):
    merged = get_active_opened_mr(project, args)
    msg = f'Open active MR since {args.since.isoformat()} with labels {",".join(args.labels)}:'
    eprint(msg)
    fmt = "{:<5} {:<24} {:>10} {:>25} {}"
    eprint(fmt.format("Id", "Updated", "State", "Title", "Labels", "Url"))
    for m in merged:
        eprint(
            fmt.format(
                m.iid,
                m.updated_at,
                m.state,
                m.title,
                ",".join(m.labels),
                url_issue(m),
            )
        )


ACTIONS = {
    'mr-candidates': action_mr_candidates,
    'mr-stats': action_mr_stats,
    'mr-merging': action_mr_merging,
    'mr-merged': action_mr_merged,
    'mr-closed': action_mr_closed,
    'mr-active-open': action_mr_active_open,
    'issues-active': action_issues_active,
}


def valid_date(date_str):
    try:
        return datetime.strptime(date_str, "%Y-%m-%d")
    except ValueError as e:
        msg = "Not a valid date: '{0}'.".format(date_str)
        raise argparse.ArgumentTypeError(msg) from e


def main():
    parser = argparse.ArgumentParser(description="Gitlab mr dispatcher tools")
    parser.add_argument(
        "action",
        type=str,
        help="""
        mr-candidates: show non-WIP  MRs, approved, with no open threads.
        mr-stats: summarize workload on mr team members.
        mr-merging: show any MRs set to mr when pipeline succeeds.
        mr-merged: shows merged MRs since a certain date with a certain label.
        mr-closed: shows closed MRs since a certain date with a certain label.
        issues-active: shows issues active since a given date  with a given label.
        """,
        choices=list(ACTIONS.keys()),
    )

    parser.add_argument(
        "--since",
        help="show only resources whose state change date superior than this value on the format Y-m-d",
        dest='since',
        default=None,
        type=valid_date,
    )

    parser.add_argument(
        "--label",
        help="show only resources with this label. can be supplied multiple times (intersection)",
        dest='labels',
        nargs='+',
    )

    args = parser.parse_args()

    glab = init_gitlab()

    # Get a project by ID
    project = glab.projects.get(PROJECT_ID)

    ACTIONS[args.action](project, args)


if __name__ == "__main__":
    main()
