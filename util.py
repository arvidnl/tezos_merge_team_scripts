import sys
import gitlab  # type: ignore


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def init_gitlab():
    glab = gitlab.Gitlab.from_config('gitlab', ['gl.cfg'])

    glab.auth()

    return glab


def url_mr(resource):
    return f'http://gitlab.com/tezos/tezos/merge_requests/{resource.iid}'


def url_issue(resource):
    return f'http://gitlab.com/tezos/tezos/issues/{resource.iid}'
