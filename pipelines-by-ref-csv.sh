ref=$1

for id in `curl -s "https://gitlab.com/api/v4/projects/22795010/pipelines?ref={$ref}" | jq -r 'map(.id) | .[]'`; do curl -s "https://gitlab.com/api/v4/projects/22795010/pipelines/$id" | jq -r '[.id, .ref, .status, .duration] | @csv'; done
