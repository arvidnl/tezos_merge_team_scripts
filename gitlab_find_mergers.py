import gitlab
import argparse


# TODO:
#  - also find approvers
#  - people merge stuff to which they're not assigned. indeed this is
#    probably the norm. there is api to filter MRs on merger so would
#    have to download all MRs to check.
#  - Benjamin and Sylvain R are missing from the results
#


def init():
    gl = gitlab.Gitlab.from_config('gitlab', ['gl.cfg'])

    # make an API request to create the gl.user object. This is mandatory
    # if you use the username/password authentication.
    gl.auth()

    return gl


def find_mergers_of(gl, label):
    print(f"Find mergers of {label}...")

    # Get a project by ID
    project_id = 3836952
    project = gl.projects.get(project_id)

    mrs = project.mergerequests.list(state='merged', labels=label)
    if len(mrs) == 0:
        print(f"Found no merged MRs of label {label}")
        return

    mergers = {}
    for mr in mrs:
        merger = mr.merged_by['name']
        if merger not in mergers:
            mergers[merger] = []
        mergers[mr.merged_by['name']].append(mr)
        print("!%d merged by: %s" % (mr.iid, mr.merged_by['name']))

    for merger, mrs in mergers.items():
        print("%s: %d" % (merger, len(mrs)))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Gitlab merge master.')
    parser.add_argument(
        'label', metavar='label', help='label to finder mergers of'
    )
    args = parser.parse_args()
    gl = init()
    find_mergers_of(gl, args.label)
